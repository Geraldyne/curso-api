<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\CursoSuscripcion;
use App\Traits\ApiResponser;
use App\Curso;
use App\CursoCliente;
use Exception;
use Notification;

class CursoClienteController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();        
        
        $suscripciones = $user->cursos()->with('curso')->get();

        return $this->showAll([
                'cursos' => $suscripciones
            ], 200);
    }


    public function suscrito(Request $request, $curso_id){

        $user = Auth::user();

        $suscripcion = CursoCliente::where(["curso_id" => $curso_id, "user_id" => $user->id])->first();

        return $this->successResponse([
                        'curso' => $suscripcion,
                    ], 200);

    }


    public function suscribir(Request $request)
    {
        $user = Auth::user();

        $curso = Curso::find($request->curso_id);

        if($curso){

            if($user->hasCurso($curso)){
                return $this->errorResponse("Ya esta suscrito a este curso", 409 ); 
            }

            try{

                $suscripcion = new CursoCliente;
                $suscripcion->curso_id = $curso->id;
                $suscripcion->user_id = $user->id;
                $suscripcion->save();

                $email_enviado = $this->sendMail($user, $curso);

                return $this->successResponse([
                        'msg' => "Se ha suscrito exitosamente",
                        'email_enviado' => $email_enviado
                    ], 200);

            } catch (Exception $e) {

                return $this->errorResponse("No se ha podido realizar la suscripción en este momento", 501); 

            }

        }

        return $this->errorResponse("Curso no encontrado", 404); 

    }


    public function cancelarSuscripcion(Request $request)
    {

        $suscripcion = CursoCliente::find($request->id);

        if($suscripcion){

            try{

                $suscripcion->delete();

                return $this->successResponse([
                        'msg' => "Se ha cancelado la suscripción exitosamente"
                    ], 200);

            } catch (Exception $e) {

                return $this->errorResponse("No se ha podido cancelar la suscripción en este momento", 501); 

            }

        }

        return $this->errorResponse("Curso no encontrado", 404); 

    }


    public function sendMail($user, $curso){

        try {

            $from = config('mail.username');

            $user->notify(
                new CursoSuscripcion($user, $curso)
            );

            return true;

        }
        catch (Exception $e) {

            return false;

        }

    }

}
