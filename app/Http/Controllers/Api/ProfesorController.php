<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CreateProfesorRequest;
use App\Http\Requests\UpdateProfesorRequest;
use App\Traits\ApiResponser;
use App\Profesor;
use Exception;

class ProfesorController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profesor = Profesor::all();
        
        return $this->showAll([
            'profesor' => $profesor
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProfesorRequest $request)
    {
        
        if(Profesor::where('cedula', '=', $request->cedula)->exists()) 
            return $this->errorResponse("El profesor ya existe", 409 );

        try {
            
            $profesor  = new Profesor;
            
            $profesor->fill($request->all());
           
            $profesor->save();
            

            return $this->successResponse([
                'profesor' => $profesor
            ], 200);

        } catch (Exception $e) {
            return $e->getMessage();
            return $this->errorResponse("Error al registrar al profesor", 501); 
            
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Profesor $profesor)
    {
        if($profesor) {
            return $this->successResponse([
                'profesor' => $profesor
            ], 200);
        }

        return $this->errorResponse("Profesor no encontrado", 501); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Profesor $profesor)
    {
        $profe = Profesor::findOrFail($profesor);

        return $this->successResponse([
            'profesor' => $profe
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfesorRequest $request, Profesor $profesor)
    {
    
        try {
        
            $profesor->update( $request->all() );
            
            return $this->successResponse([
                'profesor' => $profesor
            ], 200);

        } catch (Exception $e) {
            return $this->errorResponse("Error al actualizar datos del profesor", 501); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profesor $profesor)
    {

        foreach($profesor->cursos as $curso){
            $curso->profesor_id = null;
            $curso->save();
        }

        try{

            $profesor->delete();

            return $this->successResponse([
                    'msg' => "Profesor eliminado satisfactoriamente"
                ], 200);

        } catch (Exception $e) {
            return $this->errorResponse("Error al eliminar profesor", 501); 
        }
        
    }
}
