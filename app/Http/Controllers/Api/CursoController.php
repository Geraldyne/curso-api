<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CreateCursoRequest;
use App\Http\Requests\UpdateCursoRequest;
use App\Traits\ApiResponser;
use App\Curso;
use Exception;

class CursoController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $user = Auth::user();

        if($user->rol == "administrador"){
            
            $cursos = Curso::all();

        }
        else{

            $cursos = Curso::filter($user, $request->all());

        }
        
        
        return $this->showAll([
                'cursos' => $cursos
            ], 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCursoRequest $request)
    {
        
        if(Curso::where('nombre', '=', $request->nombre)->exists()) 
            return $this->errorResponse("El curso ya existe", 409 );

        try {

            $curso  = new Curso();
            $curso->fill($request->all());
            $curso->save();

            return $this->successResponse([
                    'curso' => $curso
                ], 200);

        } catch (Exception $e) {

            return $this->errorResponse("Error al crear curso", 501); 
            
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Curso $curso)
    {
        if($curso){
            return $this->successResponse([
                'curso' => $curso
            ], 200);
        }

        return $this->errorResponse("Curso no encontrado", 501); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Curso $curso)
    {
        $curso = Curso::findOrFail($curso);

        return $this->successResponse([
                'curso' => $curso
            ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCursoRequest $request, Curso $curso)
    {
        
        try {
        
            $curso->update( $request->all() );
            return $this->successResponse([
                    'curso' => $curso
                ], 200);

        } catch (Exception $e) {
            return $this->errorResponse("Error al actualizars curso", 501); 
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Curso $curso)
    {
        try{

            $curso->delete();

            return $this->successResponse([
                    'msg' => "Curso eliminado satisfactoriamente"
                ], 200);

        } catch (Exception $e) {
            return $this->errorResponse("Error al eliminar curso", 501); 
        }
    }
}