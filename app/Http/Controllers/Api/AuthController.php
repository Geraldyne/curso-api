<?php

namespace App\Http\Controllers\Api;

use App\User;
use Validator;
use App\PushNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class AuthController extends Controller
{

    public function index()
    {
        $users = User::all();
        
        return response()->json([
            'user' => $users
        ], 200);
    }
 
    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        

        if (Auth::attempt($request->only('email', 'password'))) {
            $user = Auth::user();
            $token =  $user->createToken(config('app.name'))->accessToken;

            return response()->json([
                'token' => $token,
                'user' => $user
            ], 200);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    
}
