<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Curso extends Model
{
    protected $table = 'cursos';
    
    protected $fillable = ['nombre','descripcion','precio','categoria','profesor_id'];

    protected $appends = ["profesor_nombre", "precio_currency"];

    public function profesor()
    {
        return $this->belongsTo(Profesor::class, 'profesor_id');
    }


    public function getPrecioCurrencyAttribute(){
        if($this->precio){
            return number_format($this->precio, 2, ',', '.') . ' $';
        }
        return 0 . ' $';
    }


    public function getProfesorNombreAttribute()
    {
        return $this->profesor ? $this->profesor->nombre . ' ' . $this->profesor->apellido : "Por Asignar";
    }


    public static function filter($user, array $options = []){

        $cursos = Curso::orderBy('id', 'desc');

        if ( isset($options['categoria']) ) {
            $busqueda = $options['categoria'];

            $cursos->conCategoria($busqueda);
        }

        $cursos->select('cursos.*','cursos_clientes.id as curso_cliente_id', 'cursos_clientes.user_id')
            ->leftJoin('cursos_clientes', function($query)use($user){

                $query->on('cursos.id', '=', 'cursos_clientes.curso_id');
                $query->on(DB::raw('cursos_clientes.user_id'), DB::raw('='),DB::raw("'".$user->id."'"));

            });
        
        return $cursos->get();
    }


    public function scopeConCategoria($query, $categoria = null)
    {
        return $query->orWhere('categoria', 'like', "%$categoria%");
    }

}
