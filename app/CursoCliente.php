<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CursoCliente extends Model
{
    protected $table = 'cursos_clientes';

    protected $fillable = ['curso_id','user_id'];

    public function curso()
    {
        return $this->belongsTo(Curso::class, 'curso_id');
    }

    public function clientes()
    {
        return $this->hasMany(CursoCliente::class);
    }
}
