<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesor extends Model
{
    protected $table = 'profesores';

    protected $fillable = ['nombre','apellido','cedula','correo'];

    public function cursos()
    {
        return $this->hasMany(Curso::class);
    }

}
