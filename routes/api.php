<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api'], function () {
	
    Route::post('login', 'AuthController@login'); 


    Route::group(['middleware' => 'auth:api'], function () {

		Route::resource('cursos', 'CursoController', ['parameters' => [
	        'cursos' => 'curso'
	    ]]);
	    
	    Route::resource('profesores', 'ProfesorController', ['parameters' => [
	        'profesores' => 'profesor'
	    ]]);
	    
	    Route::get('mis-cursos', 'CursoClienteController@index');

		Route::get('mi-curso/{curso_id}', 'CursoClienteController@suscrito');

	    Route::post('suscribirme', 'CursoClienteController@suscribir');

	    Route::post('cancelar-suscripcion', 'CursoClienteController@cancelarSuscripcion');
	});

    
});

