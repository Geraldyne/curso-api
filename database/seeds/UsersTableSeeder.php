<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@cursos.com',
            'password' =>Hash::make('admin'),
            'rol' => 'administrador'
        ]);
        DB::table('users')->insert([
            'name' => 'cliente',
            'email' => 'cliente@cursos.com',
            'password' => Hash::make('cliente'),
            'rol' => 'cliente'
        ]);

        DB::table('users')->insert([
            'name' => 'cliente2',
            'email' => 'cliente2@cursos.com',
            'password' => Hash::make('cliente'),
            'rol' => 'cliente'
        ]);

    }
}
