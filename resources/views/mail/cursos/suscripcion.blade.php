@component('mail::message')

# Querido {{$user->name}}.

<br>

@component('mail::panel')
    Gracias por suscribirte a nuestro curso {{$curso->nombre}}!!
@endcomponent

@endcomponent
